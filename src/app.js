const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors'); //look at serving up client as route on this and sending down public index.html file instead of adding cors rules.
const Claim = require('../models/Claim');
require('now-env');

const app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cors());

const { MONGO_URL, PORT } = process.env;

mongoose.connect(MONGO_URL, { useNewUrlParser: true });

app.listen(PORT || 8081);

app.get('/claims', (req, res) => {
	Claim.find()
		.then(claims => res.send(claims))
		.catch(err => res.status(400).send());
});

app.get('/claims/:id', (req, res) => {
	Claim.findById(req.params.id)
		.then(claim => res.send({ claim }))
		.catch(error => res.status(400).send(error));
});

app.post('/claims', (req, res) => {
	const { reason, amount } = req.body;
	new Claim({ reason, amount })
		.save()
		.then(claim => res.send({ success: true }))
		.catch(error => res.status(400).send(error));
});

app.put('/claims/:id', (req, res) => {
	console.log('put', req.params.id, req.body);
	const { reason, amount } = req.body;
	Claim.findByIdAndUpdate(
		req.params.id,
		{ reason, amount },
		{ upsert: true, new: true }
	)
		.then(claim => res.send(claim)) //might make more sense to return success boolean
		.catch(error => res.status(400).send(error));
});

app.delete('/claims/:id', (req, res) => {
	Claim.findByIdAndRemove(req.params.id)
		.then(() => res.send({ success: true }))
		.catch(error => res.status(400).send(error));
});
