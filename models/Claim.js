const mongoose = require('mongoose');

const Claim = mongoose.model('Claim', {
	reason: {
		type: String,
		required: true,
		trim: true,
		minlength: 3
	},
	amount: {
		type: Number,
		required: true
	}
});

module.exports = Claim;
